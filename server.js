const port = 5000



const express = require('express') 

const app = express() 

app.use(express.static('dist/spa'))

app.listen(port, () => console.log(`Listening on port ${port}`))

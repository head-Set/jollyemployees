// import Vue from 'vue'
import axios from 'axios'

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/boot-files
const api = axios.create({
  baseURL: 'http://localhost:8080/employee',
  // baseURL: 'http://178.128.51.242:8088/employee',

});
export default ({ app, store, router, Vue }) => {
  api.interceptors.request.use(config => {
    config.headers['Authorization'] = store.state.Auth.token;
    console.log('====================================');
    console.log('Making request to ' + config.url);
    console.log('====================================');
    return config;
  }, error => {
    console.log("Axios Error request", error.request)
    return Promise.reject(error)
  })
  api.interceptors.response.use(response => {
    return response;
  }, error => {
    console.log('====================================');
    console.log(error);
    console.log('====================================');
    let err;
    if (error.response) {

      if (error.response.status === 401) {
        // $store.replaceState({
        //     token: "",
        //     role: "",
        //     user: ""
        // })
        // app.$auth.reset();
        // localStorage.clear()
        store.dispatch("Auth/logout")
        // redirect("/login")

      }
      err = error.response.data;
    }
    else if (error.request) {
      err = new Error("Error Occured")
    }
    else {
      err = new Error("Error Occured")
    }

    return Promise.reject(err);
  })
  Vue.prototype.$axios = api
};
export {
  api
}

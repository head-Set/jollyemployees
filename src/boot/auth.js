// import something here

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/boot-files
export default async ({ app, store, router, Vue }) => {
  router.beforeEach((to, from, next) => {
    const token = store.state.Auth.token;
    console.log(to)
    try {
      if (to.matched.some(record => record.meta.auth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.


        if (!token) {
          next({
            path: '/',
            query: { replace: to.fullPath }
          })
        } else {
          next()
        }
      } else {
        if (token) {
          next("/home")
        } else {
          if (to.path != '/')
            next({
              path: '/',
              query: { replace: to.fullPath }
            })
          else
            next()

        }
      }

    } catch (error) {
      console.log(error);
      if (token) {
        next()
      } else {
        next({
          path: '/',
          query: { replace: to.fullPath }
        })

      }
    }

  })
};

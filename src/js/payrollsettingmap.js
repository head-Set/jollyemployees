export default {
    special_holiday: "Special Holiday",

    legal_holiday: "Legal Holiday",

    night_differential: "Night Differential",

    night_differential_special_holiday: "Night Differential Special Holiday",

    night_differential_legal_holiday: "Night Differential Legal Holiday",

    regular_overtime: "Overtime",

    special_holiday_overtime: "Special Holiday Overtime",

    legal_holiday_overtime: "Legal Holiday Overtime",

    night_differential_overtime: "Night Differential Overtime",

    night_differential_special_holiday_overtime: "Night Differential Special Holiday Overtime",

    night_differential_legal_holiday_overtime: "Night Differential Legal Holiday Overtime",

    rest_day: "Rest Day",

    rest_day_special_holiday: "Rest Day Special Holiday",

    rest_day_legal_holiday: "Rest Day Legal Holiday",

    rest_day_night_differential: "Rest Day Night Differential",

    rest_day_night_differential_special_holiday: "Rest Day Night Differential Special Holiday",

    rest_day_night_differential_legal_holiday: "Rest Day Night Differential Legal Holiday",

    rest_day_overtime: "Rest Day Overtime",

    rest_day_special_holiday_overtime: "Rest Day Special Holiday Overtime",

    rest_day_legal_holiday_overtime: "Rest Day Legal Holiday Overtime",

    rest_day_night_differential_overtime: "Rest Day Night Differential Overtime",

    rest_day_night_differential_special_holiday_overtime: "Rest Day Night Differential Special Holiday Overtime",

    rest_day_night_differential_legal_holiday_overtime: "Rest Day Night Differential Legal Holiday Overtime",

    regular: "Regular",

    regular_work: "Regular",


    late_undertime: "Late/Undertime"
    
}
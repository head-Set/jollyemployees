import { Notify, Loading } from 'quasar'
import { api } from '../../boot/axios';
export async function get({ commit }) {
    try {
        commit('SET_LOADING', true);

        const response = await api.get('/dtrs')


        commit('SET_DATA', response.data);

        commit('SET_LOADING', false);

    } catch (error) {
        commit('SET_LOADING', false);

        console.log(error.message ? error.message : 'Failed Get Dtrs');
    }


}

export async function dtrs({ commit }, period) {
    try {
        commit('SET_DTRS_LOADING', true);
        Loading.show();
        const response = await api.get(`/dtrs/${period}`)


        commit('SET_DTRS', response.data);

        commit('SET_DTRS_LOADING', false);
        Loading.hide();

    } catch (error) {
        commit('SET_DTRS_LOADING', false);
        Loading.hide();
        Notify.create({
            type: 'negative',
            icon: 'error',
            message: error.message ? error.message : `Failed to get ${period} Dtrs`
        })
        console.log(error.message ? error.message : `Failed to get ${period} Dtrs`);
    }


}
export default function () {
  return {
    data: [],
    loading: false,
    dtrs_data: {
      computed_dtr: {},
      total_dtrs_breakdown: {},
      daysworked: {},
    },
    dtrs_loading: false,
  }
}

export function SET_DATA(state, data) {

    state.data = data
}

export function SET_LOADING(state, data) {

    state.loading = data
}

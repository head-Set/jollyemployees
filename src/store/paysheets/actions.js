import { Notify, Loading } from 'quasar'
import { api } from '../../boot/axios';
export async function get({ commit }) {
    try {
        commit('SET_LOADING', true);

        const response = await api('/paysheets')


        commit('SET_DATA', response.data);

        commit('SET_LOADING', false);

    } catch (error) {
        commit('SET_LOADING', false);

        console.log(error.message ? error.message : 'Failed Get Paysheets');
    }


}

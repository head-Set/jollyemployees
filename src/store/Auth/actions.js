import { api } from '../../boot/axios';
// import api from '../../boot/axios';
import { Notify, Loading } from 'quasar'
export async function SignIn({ commit, dispatch }, data) {

  try {
    Loading.show();
    const response = await api.post('/login', data)
    commit('setMyProfile', response.data.user);
    commit('setToken', response.data.token);
    this.$router.push('/home');
    Loading.hide();

  } catch (error) {
    Loading.hide();
    Notify.create({
      type: 'negative',
      icon: 'error',
      message: error.message ? error.message : 'Failed to login'
    })
  }
}

export function logout({ commit }) {
  Loading.show();
  Loading.hide();

  commit('setMyProfile', []);
  commit('setToken', "");
  this.$router.push('/')
}

export function getMyDepartment({ commit }) {
  api('/department')
    .then(response => {
      commit('setMyDepartment', response.data);
    }, e => {
      Notify.create({
        type: 'negative',
        icon: 'error',
        message: 'Failed to to Get Department'
      })
    }).catch(error => {
      console.log(error.message ? error.message : 'Failed Get Department');
    });
}

export async function ChangePassword({ state, commit }, data) {


  try {
    Loading.show();
    const response = await api.put('/changepass', data)
    Notify.create({
      type: 'positive',
      icon: 'done',
      message: "Password Changed"
    });

    commit('setToken', response.data.token);
    Loading.hide();

  } catch (error) {
    Loading.hide();
    Notify.create({
      type: 'negative',
      icon: 'error',
      message: error.message ? error.message : 'Failed to change password'
    })
  }
}
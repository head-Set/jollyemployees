
const routes = [
  {
    path: '/',
    component: () => import('layouts/IndexLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Signin.vue') }
    ]
  },
  {
    path: '/home',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/dtr/:period', component: () => import('pages/DtrDetails/Index.vue') },
      { path: '/paysheet', component: () => import('pages/PaySheet.vue') },
      { path: '/paysheet/:period', component: () => import('pages/PaysheetDetails/index.vue') },
      { path: '/me', component: () => import('pages/Profile.vue') },
    ],
    meta: {
      auth: true
    }
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
